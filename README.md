# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : CCPOSTER(Forum)
* Key functions (add/delete)
    1. user page
    2. post page (post list page)
    3. leave comments below posts
    
* Other functions (add/delete)
    1. users can choose a icon when they sign up.
    2. users can change pages in the post list.
    3. it(forum) provides different boards for different theme.
    4. in the post list page, the bar on the left side provides the overview of posts.

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

**Access CCPOSTER and trigger the fusion of trend!**

CCPOSTER is a forum with different boards(NTHU, FOOD, FUNS, MUSIC, GAME, SPORT, SCHOOL, GHOOST, 3C).
Users can sign up to see the post and leave comments with different devices.

| laptop | cellphone |
| ------ | --------- |
| <img src="https://i.imgur.com/tRTB8Ax.png" width="192px" height="113px"></img> | <img src="https://i.imgur.com/ryrcUU1.png" width="108px" height="192px"></img> |


**Check out what happended today in your school.**

Login and go to NTHU page to see the comments.

| login page |
| ------ |
| <img src="https://i.imgur.com/X0v3N5Q.png" width="384px" height="226px"></img> |

| board page |
| ------ |
| <img src="https://i.imgur.com/7nTZvtu.png" width="384px" height="226px"></img> |

**Share your colorful life with other users.**

Press the "+" button on the top right of the title bar to add new post.

| board page |
| ------ |
| <img src="https://i.imgur.com/t5taImm.png" width="384px" height="226px"></img> |

**Handle attacks**

It will block HTML code.

| error message |
| ------ |
| <img src="https://i.imgur.com/I4Kl7cO.png" width="384px" height="226px"></img> |


# 作品網址：https://midterm-project-73211.firebaseapp.com/

# Components Description : 
1. Key Function (user page): Click the user icon or email(button) on the top of the page to check the user page.
2. Key Function (post page): Click one cube(board) on the home page(index.html) and get into the post page. Users can press the "+" button on the top right of the title bar to add new post.
3. Key Function (leave comments) : Click the post title and you will activate the collapse and see the comment bar below.
4. Membership Mechanism : You can sign up and login. (click the Login button on the top of the page.)
5. Firebase Page : You can check the URL (firebase).
6. Database : You can only read/write the data when you are authenticated (login).
7. RWD : You can change the size of page to test RWD (it is more clear on cellphone).
8. Chrome Notification : You can click the button (Say Hi!) on the user page to get chrone notification.
9. Use CSS Animation : The cube animation on main page and User page.
10. Security : You can try to input HTML code when you leave comment, post or when you want to login. You will get the rejected message.
11. Third-Party Sign In : You can login with google account.



# Other Functions Description(1~10%) : 
1. user icon : users can choose a icon when they sign up.
2. change pages : The maximum number of posts in a page is 10. Users can click "previous" or "next" button to change page.
3. different boards : Users can see different themes of post by accessing different board.
4. side bar : To provide information more clearly, in the post list page, there is a bar with the overview of posts.

## Security Report (Optional)
1. DOM attack: You will receive Modal notification when you are trying to input HTML code. The mechenism to avoid HTML code injection is a function called "sanitizeHTML()"(You can check it in board.js and signin.js).
I create a new div and set 'textContent' = input string and then take its innerHTML to get sanitized string.
2. Query String attack: Users may input text in the query string and that would make the board page disordered. I use a function called "access_page(page_name)" to eliminate illegal query strings. So if you access the board page directly, you may get "404 not found".

## Reference
- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_pills_dynamic&stacked=h
- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_form_horizontal&stacked=h
- https://www.w3schools.com/css/tryit.asp?filename=tryresponsive_col-s
- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_scrollspy2&stacked=h
- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_form_textarea&stacked=h
- https://redstapler.co/pure-css-3d-cube-effect-tutorial/
- https://gomakethings.com/preventing-cross-site-scripting-attacks-when-using-innerhtml-in-vanilla-javascript/
- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_collapsible_accordion&stacked=h
- https://stackoverflow.com/questions/2271156/chrome-desktop-notification-example

NOTE: some posts are copied from Dcard.
